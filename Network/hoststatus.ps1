# Pings hosts for user defined length of time
# Writes results to csv after each round of pings


function Get-ConnectionStatistics {
    [CmdletBinding()]  
    Param (   
        [Parameter(Mandatory = $true, Position = 0)]
        [ValidateNotNullOrEmpty()]
        [array]$Hosts,
        [string]$FilePath = "C:\users\$([System.Environment]::UserName)\Desktop\pingresult.csv",
        [int]$SleepTime = 0,
        [int]$PingTime = 1
    )  
    Remove-Item -Path $FilePath
    $Header_select = @("TIME")
    $Header_select += $Hosts
    $Header = "" | Select-Object $Header_select
    $Header | Export-Csv -Path $FilePath -NoTypeInformation


    $AddTime = (Get-Date).AddMinutes($PingTime)
    while ((Get-Date) -le $AddTime){
        Start-Sleep $SleepTime
        $pingresult = "" | Select-Object $Header_select
  
        $pingresult.Time = (Get-Date).ToString('yyyy-MM-dd HH:mm:ss')

        $Hosts | ForEach-Object {
            $computer = $_
            Try{
                $ping = Test-Connection -ComputerName $computer -Count 1 -BufferSize 32 -ErrorAction Stop
                if($ping){
                    try {

                        if($ping.Status -like "Success") {
                            $pingresult.$computer = $ping.Latency[0]
                        }
                        elseif ($Ping.Status -like "TimedOut") {
                            $pingresult.$computer = "Timed Out"
                        }
                        else{
                            $pingresult.$computer = "UnChecked Status"
                        }
                        
                    }
                    catch{
                        $pingresult.$computer = "Error."
                    }
                }
                
            }
            catch{
                $pingresult.$Computer = "Could not Resolve Host"
            }
        }
        $pingresult | export-CSV -Path $FilePath -NoTypeInformation -Append
        
       
    }
}
Write-Host "1. Enter Hosts"
Write-Host "2. Import Host file"
$x = Read-Host -Prompt "Choose"
    
switch ($x) {
    1 { 
        $pcnames    = (Read-host -Prompt "Hosts").split(',')
     }
    2 {
        Write-Host "TXT file with each host on a new line"
        Get-Content -path (Read-Host -prompt "Path")
    } 
    Default {
        Write-Host "Wrong selection. Exiting..."
        Pause;exit
    }
}




if($(Read-host -prompt "Set a delay between pings? [Y/N]") -eq "y"){
    [int]$SleepTime  = Read-Host -Prompt "Delay time between pings"
}
Else{
    $SleepTime  = 0
}
if($(Read-host -prompt "Set a runtime? Process will stop when time runs out. [Y/N]") -eq "y"){
    [int]$PingTime   = Read-Host -Prompt "Ping hosts for how long in minutes" 
}
Else{
    # One year
    $PingTime   = 512640
}
Get-ConnectionStatistics -Hosts $pcnames -SleepTime $SleepTime -PingTime $PingTime

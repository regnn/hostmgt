# Retrives IP configuration from Registry

$RegPath = "HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces"
$IPConfigs = @()    
Get-ChildItem -Path $RegPath | ForEach-Object{
    
    [String]$NewRegPath = "Registry::" +  $_.Name
    $Row = Get-ItemProperty -Path $NewRegPath
    $IPConfigs += $Row
}

# Selects and Converts desired fields as strings
# If you add a field and see System.String[] you need to join as a string
$IPTable = $IPConfigs | `
    Select-Object `
        EnableDHCP,`
        @{N="IPAddress";E={[string]::Join(",",$_.IPAddress)}},@{N="SubnetMask";E={[string]::Join(",",$_.SubnetMask)}},`
        @{N="DefaultGateway";E={[string]::Join(",",$_.DefaultGateway)}},@{N="NameServer";E={[string]::Join(",",$_.NameServer)}},`
        @{N="DhcpIpAddress";E={[string]::Join(",",$_.DhcpIpAddress)}},@{N="DhcpSubnetMask";E={[string]::Join(",",$_.DhcpSubnetMask)}},`
        @{N="DhcpDefaultGateway";E={[string]::Join(",",$_.DhcpDefaultGateway)}},@{N="DhcpNameServer";E={[string]::Join(",",$_.DhcpNameServer)}}
$IPTable | Format-Table
if ($(Read-Host -Prompt "Enter [Y] to export") -eq "Y") {
    Write-Host "Exporting to working desktop"
    $CurrentUser = [System.Environment]::UserName
    $IPTable | Export-Csv -Path "C:\Users\$CurrentUser\Desktop\IPConfigs.csv" -NoTypeInformation -Delimiter ","
}



$diagonstic_report = "C:\Temp\network_diagnostics_$((Get-Date).ToString('yyyyMMdd_HHmmss')).txt"
$hostname = "www.google.com"

$bandwidth_report = "C:\Temp\network_usage_$adapter.txt"
$adapter = "Ethernet"
$max_intervals = 3




$results = @()
$results += Test-Connection -ComputerName $hostname -Count 4
$results += Get-NetIPConfiguration
$results += Get-NetAdapterStatistics
$results += Test-NetConnection -ComputerName $hostname -TraceRoute

$results | Out-File -FilePath $diagonstic_report


$interval = 0
while ($interval -le $max_intervals) {
    Start-Sleep -Seconds 60
    $stats = Get-NetAdapterStatistics -Name $adapter
    $timestamp = Get-Date
    $output = "Timestamp: $timestamp`nBytes Received: $($stats.ReceivedBytes)`nBytes Sent: $($stats.SentBytes)`n"
    Add-Content -Path $bandwidth_report -Value $output
    $interval += 1
}

# DFS Reports
#
#

## Mail 
#
$mail_from = ""
$mail_to = ""
$mail_subject = "DFSR Health Report - $((get-date).ToString('MM/dd/yyyy'))"
$mail_encoding = ""
$mail_smtp = """
$output = @()

## DFS
#
$GroupName = ""
$SourceComputerName = ""
$DestinationComputerName = ""

<#
## Gets all Folders within a replication group and  returns their current backlog as a table
#>
function Get-FilesBacklog {  
    
    $DFSreport = @()
    Get-DfsReplicatedFolder -GroupName $GroupName | ForEach-Object {
        $Row = "" | Select-Object FolderName,State,Backlog
        $Row.FolderName = $_.FolderName
        $Row.State      = $_.State
        $Row.Backlog    = " Count: $((Get-DfsrBacklog -GroupName $GroupName -FolderName $_.foldername -SourceComputerName $SourceComputerName -DestinationComputerName $DestinationComputerName -Verbose 4>&1).Message.Split(':')[2])"
        $DFSreport      += $Row
    }
    return $DFSreport
}

function Send-AdminMessage{
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true, Position = 0)]
        [ValidateNotNullOrEmpty()]
        $MailBody
    )

    $Recipient      = $mail_to
    $MailSender     = $mail_from
    $Subject        = $mail_subject
    $SmtpServer     = $mail_smtp
    $MailEncoding   = $mail_encoding
    
    Send-MailMessage -To $Recipient -From $MailSender -Subject $Subject -SmtpServer $SmtpServer -Encoding $MailEncoding -Body $MailBody -BodyAsHtml
    
}


<#
## MAIN
#>

Send-AdminMessage -MailBody $(Get-FilesBacklog | Format-Table -auto | ConvertTo-Html)

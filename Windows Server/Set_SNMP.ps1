<#
Installs SNMP and sets community and Permitted Manager
If you are removing WSUS (Win 10/11 install) checks for internet access
#>

$PermittedManagers =""
$community = ""
$CommunityRights = 4    # 4 = READ ONLY

# Remove WSUS Conditional
:RemoveWsus while($true) {
    switch ($(Read-Host -Prompt "Remove WSUS Registry Entries? [Y/N]")) {
        "Y" {

            if ((test-netconnection google.com -port 80).TcpTestSucceeded -eq $false)  {
                write-host "You do not have internet access"
                Write-Host "If you are removing wsus. You should have internet."
                Pause
                exit
            }

            try {
                Stop-Service -Name wuauserv
                Remove-Item -path HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate -Recurse
                Start-Service -Name wuauserv
            }
            catch {
                Write-Host "Failed Reg changes. Attempting to fix the issue."
            
                Stop-Service -Name wuauserv
                New-Item -Path HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate\AU -Name UseWUServ -Value 0 -Force
                Start-Service -Name wuauserv
                Start-Sleep 10
                Stop-Service -Name wuauserv
                Remove-Item -path HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate -Recurse
                Start-Service -Name wuauserv
            }
            break RemoveWsus
        }
        "N" {
            break RemoveWsus
        }
        Default {clear-host}
    }
}

# Install SNMP
write-host "Checking Windows Product Name"
if((get-computerinfo).WindowsProductName -notlike "Windows Server*"){
    # Install SNMP
    Get-WindowsCapability -Online -Name *SNMP* | ForEach-Object{
        Write-Host $_.Name
        Add-WindowsCapability -Online -Name $_.Name
    }
}
else{
    Install-WindowsFeature SNMP-Service -IncludeAllSubFeature -IncludeManagementTools
}


# Set community Name and Manager IPv4
New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\SNMP\Parameters\PermittedManagers -Name 2 -PropertyType "STRING" -Value $PermittedManagers -Force
New-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\SNMP\Parameters\ValidCommunities -Name $community -PropertyType "DWORD" -Value $CommunityRights -Force
gpupdate /target:computer /force

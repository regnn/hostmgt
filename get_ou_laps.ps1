# Allows user to read/reset LAPS passwords on a single or OU basis
# Allows user to export CSV of OU LAPS passwords

#OU where PC is stored
$SEARCHBASE = ''


# Opens a GUI window to select what folder to export to
Function Select-FolderDialog{
    param([string]$Description="Select Folder",[string]$RootFolder="Desktop")

 [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") |
     Out-Null     

   $objForm = New-Object System.Windows.Forms.FolderBrowserDialog
        $objForm.Rootfolder = $RootFolder
        $objForm.Description = $Description
        $Show = $objForm.ShowDialog()
        If ($Show -eq "OK")
        {
            Return $objForm.SelectedPath
        }
        Else
        {
            Write-Error "Operation cancelled by user."
        }
}

While($True){
    Clear-Host
    Write-Host "1. Get an entire OU"
    Write-Host "2. Get a single PC"
    Write-Host "0. Exit"
    $x2 = Read-Host -Prompt "Choose"

    switch ($x2) {
        1 { 
            $OU = $(Get-ADOrganizationalUnit -LDAPFilter '(name=*)' -SearchBase $SEARCHBASE | Out-GridView -PassThru)
            [string]$DN = $OU.DistinguishedName
            Get-ADComputer -Filter * -SearchBase $DN -Properties description,IPv4Address, ms-Mcs-AdmPwd, ms-Mcs-AdmPwdExpirationTime  | `
            Format-Table Name,IPv4Address,ms-Mcs-AdmPwd,@{n='AdmPwdExpirationTime';e={ [datetime]::FromFileTime($_.'ms-Mcs-AdmPwdExpirationTime')}},description

            try{
                While($True){
                    
                    Write-Host "1. Reset admin passwords"
                    Write-Host "2. Export to CSV"
                    Write-Host "0. return to previous Menu"
                    [int]$x3 = Read-Host -Prompt "Choose"
                    switch ($x3) {
                        1 {
                            Get-ADComputer -Filter * -SearchBase $DN | ForEach-Object {Reset-AdmPwdPassword $_ }
                            break
                        }
                        2{
                            $folder = Select-FolderDialog # the variable contains user folder selection
                            Get-ADComputer -Filter * -SearchBase $DN -Properties description,IPv4Address, ms-Mcs-AdmPwd, ms-Mcs-AdmPwdExpirationTime  | `
                            Select-Object Name,description,IPv4Address,ms-Mcs-AdmPwd,@{n='AdmPwdExpirationTime';e={ [datetime]::FromFileTime($_.'ms-Mcs-AdmPwdExpirationTime')}} | `
                            Export-Csv -Path "$folder\LAPS-export.csv" -NoTypeInformation
                            
                            Clear-Host
                            Write-Host "Exported to $folder\LAPS-export.csv"
                            break
                        }
                        0 {
                            break
                        }
                        Default {
                            Write-Host "Try again."
                        }
                    }
                }
            }
            catch{
                Write-Host "Sorry. Looks like you do not have permission to run the cmdlet."
            }
            Pause
        }
        2 {
            Clear-Host
            $PC = Read-Host -Prompt "Hostname"

            try{
                While($True){
                    Clear-Host
                    Get-ADComputer $PC -Properties description,IPv4Address, ms-Mcs-AdmPwd, ms-Mcs-AdmPwdExpirationTime  | `
                    Format-List Name,description,IPv4Address,ms-Mcs-AdmPwd,@{n='AdmPwdExpirationTime';e={ [datetime]::FromFileTime($_.'ms-Mcs-AdmPwdExpirationTime')}}
                    Write-Host "1. Reset admin password"
                    Write-Host "2. Copy to Clipboard"
                    Write-Host "0. return to previous Menu"
                    [int]$x4 = Read-Host -Prompt "Choose"
                    switch ($x4) {
                        1 {
                            Reset-AdmPwdPassword $PC
                            break
                        }
                        2 {
                            Get-ADComputer $PC -Properties description,IPv4Address, ms-Mcs-AdmPwd, ms-Mcs-AdmPwdExpirationTime  | `
                            Format-List Name,description,IPv4Address,ms-Mcs-AdmPwd,@{n='AdmPwdExpirationTime';e={ [datetime]::FromFileTime($_.'ms-Mcs-AdmPwdExpirationTime')}} | clip
                            break
                        }
                        0 { break }
                        Default { Write-Host "Try again."}
                    }
                    
                }
            }
            catch{
                Write-Host "Sorry. Looks like you do not have permission to run the cmdlet."
            }
            Pause
        }
        0 {
            Exit
        }
        Default {
            Write-Host "$x2 is not an option. Please choose again."
            Pause
        }
    }
}

# Onestart  Adware removal scrub script
<#
OneStart is a potentially unwanted program (PUP) that behaves like adware, often installed unknowingly through software bundling.
It redirects search queries and displays ads not originating from the websites visited
#>

# Gets current username and user sid
$user = (((Get-CimInstance -ClassName Win32_ComputerSystem).UserName).split('\')[-1])
$userSID = (Get-WmiObject -Class Win32_UserAccount -Filter "Name='$user'" | Select-Object -ExpandProperty SID)

# Kills processes and scheduled taks
Get-Process | Where-Object { $_.Name -like "onestart*" } | Stop-Process -Force
Get-ScheduledTask | Where-Object { $_.TaskName -like "onestart*" } | ForEach-Object { Unregister-ScheduledTask -TaskName $_.TaskName -Confirm:$false }


# remove application data
remove-item "C:\Users\$user\Appdata\Local\OneStart.ai" -force
remove-item "C:\Users\$user\Appdata\Roaming\Microsoft\Internet Explorer\Quick Launch\OneStart.lnk" -force
remove-item "C:\Users\$user\Appdata\Roaming\Microsoft\Windows\Start Menu\Programs\OneStart.lnk" -force
remove-item "C:\Users\$user\Desktop\OneStart.lnk" -force
Get-ChildItem -Path "C:\Windows\Prefetch" -Filter "onestart*" | remove-item -force


# Load the registry hive
$otherUserRegistryPath = "C:\Users\$user\NTUSER.DAT"  # Path to the user's NTUSER.DAT file
$loadedHivePath = "HKU\$userSID"  # The path you will mount the hive under
reg load $loadedHivePath $otherUserRegistryPath

#Remove OneStart Registry entries
$path = "Registry::HKEY_USERS\$userSID\Software\Microsoft\Windows\CurrentVersion\Run"
$items = Get-ItemProperty -Path $path

foreach ($item in $items.PSObject.Properties) {
    if ($item.Name -like "Onestart*") {
        Remove-ItemProperty -Path $path -Name $item.Name
        Write-Output "Removed $($item.Name)"
    }
}

Remove-Item -Path "Registry::HKEY_USERS\$userSID\Software\OneStart.ai"  -Recurse -Force
Remove-Item -Path "Registry::HKEY_USERS\$userSID\Software\Microsoft\Windows\CurrentVersion\Uninstall\OneStart.ai OneStart"  -Recurse -Force


$path = "Registry::HKEY_USERS\$userSID\Software\Clients\StartMenuInternet"
$items | foreach ($item in $items) {
    if ($item.PSChildName -like "onestart*") {
        Remove-Item -Path "$path\$($item.PSChildName)" -Recurse -Force
        Write-Output "Removed $($item.PSChildName)"
    }
}

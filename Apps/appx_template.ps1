# Generic System wide Offline install for AppxBundles

# Location to store local files
# Location of AppxBundle
$Masters = 'C:\Masters'
$AppBundle = ''

# Name pf AppxBundle
# Name of Appx for from Get-AppxPackage
$App = 'QuickAssist_2022.509.2259.0.AppxBundle'
$Name = "MicrosoftCorporationII.QuickAssist"

# Checks to see if the App is installed
if(Get-AppxPackage -Name $Name){
    Write-Host "$Name is already installed"
}
else {
    Write-Host "Installing $Name"
    # Checking if $Masters path exist and if $App is in it
    # If not will create the directory and/or copy item
    if(-not (Test-Path -Path "$Masters\Apps")){
        New-Item -Path $Masters -ItemType Directory -Force
    }
    if(-not (Test-Path -Path "$Masters\Apps\$App")){
        Copy-Item $AppBundle -Destination $Masters
    }

    # Installs the bundle system-wide
    DISM /online /add-provisionedappxpackage /packagepath:"$Masters\$App" /skiplicense
    
    # Local Report in $Masters
    # Creates the log file if it doesnt exist. Adds on to any existing Text
    Get-AppxPackage -Name $Name | Add-Content -Path "$Masters\logs\$Name-$(Get-Date -UFormat "%Y-%m-%d").txt"
}

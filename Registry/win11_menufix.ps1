## Returns windows 11 context menu to windows 10 style


# Define the path of the registry key
$registryPath = "HKCU:\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32"

# Create the new registry key
New-Item -Path $registryPath -Force

# Set the default value of the new key to 0
Set-ItemProperty -Path $registryPath -Name "(Default)" -Value ""
# Stops Windows Update Service removes WSUS entries in Registry then Starts Windows Update Service

Stop-Service -Name wuauserv
Remove-Item HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate -Recurse
Start-Service -Name wuauserv
